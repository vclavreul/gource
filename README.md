Usage
=====

* copy `.env.dist` to `.env` and configure it
* `docker-compose run --rm gource` for basic visualization
* `docker-compose run --rm gource --seconds-per-day 0.01` for quicker visualization

See https://github.com/acaudwell/Gource/wiki
